import json
import requests
import collections
from functools import reduce
from django.conf import settings
from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import View
from datetime import datetime, timedelta

from .models import StationDetails

class SearchWeather(View):
	template_name = 'weather_selecter.html'
	station_details = StationDetails.objects.all()

	def get(self, *args, **kwargs):
		return render(self.request, self.template_name, {'station_details': self.station_details})


	def post(self, *args, **kwars):
		select_city_station = self.request.POST.get('select_city_station', None)
		select_temp_unit = self.request.POST.get('select_temp_unit', None)
		start_date = self.request.POST.get('start_date', None)
		end_date = self.request.POST.get('end_date', None)
		start_date_obj = datetime.strptime(start_date, "%d/%B/%Y").date()
		end_date_obj = datetime.strptime(end_date, "%d/%B/%Y").date()
		delta = end_date_obj-start_date_obj
		station_details_obj = StationDetails.objects.get(pk=select_city_station)
		if delta.days > 10:
			return render(self.request, self.template_name, {'station_details': self.station_details,
				'msg': 'Can be greater than 10 days'})
		dates = start_date_obj
		result = collections.OrderedDict()
		for date_range in range(delta.days+1):
			url = "http://api.wunderground.com/api/"+settings.WUNDERGROUND_API_KEY+"/history_"+datetime.strptime(dates.strftime("%Y%B%d"), "%Y%B%d").strftime("%Y%m%d")+"/q/"+station_details_obj.country+"/"+station_details_obj.city+".json"
			wunder_response = requests.get(url)
			temp_result = []
			if select_temp_unit == "Temperature":
				count = 0
				print (url)
				for temperature in wunder_response.json()['history']['observations']:
					# if int(temperature['tempm']) < 0:
					print (temperature['tempm'])
					if temperature['tempm'] == 'N/A':
						continue
					temp_result.append(temperature['tempm'])
				result[datetime.strptime(dates.strftime("%Y%B%d"), "%Y%B%d").strftime("%Y-%m-%d")] = reduce(lambda x, y: float(x) + float(y), temp_result) / len(temp_result)
			elif select_temp_unit == "Humidity":
				for humidity in wunder_response.json()['history']['observations']:
					if humidity['hum'] == 'N/A':
						continue
					temp_result.append(humidity['hum'])
				result[datetime.strptime(dates.strftime("%Y%B%d"), "%Y%B%d").strftime("%Y-%m-%d")] = reduce(lambda x, y: float(x) + float(y), temp_result) / len(temp_result)
			dates = dates + timedelta(days=1)
			if datetime.now().date() < dates:
				break
		return HttpResponse(json.dumps(result), status=200)
