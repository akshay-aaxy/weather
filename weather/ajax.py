
from django.db import transaction
from .models import StationDetails
from django.shortcuts import render
from django.views.generic import View

class SaveStation(View):
	
	@transaction.atomic
	def post(self, *args, **kwargs):
		self.template_name = "_city_station_dropdown.html"
		country = self.request.POST.get('country', None)
		city = self.request.POST.get('city', None)
		StationDetails.objects.create(city=city, country=country)
		station_details = StationDetails.objects.all()
		return render(self.request, self.template_name, {'station_details': station_details})
