from django.conf.urls import url
from .views import SearchWeather
from .ajax import SaveStation

urlpatterns = [
    url(r'^$', SearchWeather.as_view(), name='search-weather'),
    url(r'^save-station$', SaveStation.as_view(), name='save-station'),
]
