from django.db import models

# Create your models here.
class StationDetails(models.Model):
	city = models.CharField(null=True, blank=True, max_length=255)
	country = models.CharField(null=True, blank=True, max_length=255)

	def __str__(self):
		return self.city_name+" "+station_name