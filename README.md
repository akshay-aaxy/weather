# README #

Wunderland API to fetch weather reports of the cities

### What is this repository for? ###

* This repo has code to fetch weather condition from wunderground api between ten days
* 0.0.1 V

### How do I get set up? ###

* Create a python 3 virtual environment using virtualenv -p python3 test
* Install requirements.txt file usign pip install -r requirements.txt file
* For database I have use default database and settings provide by django i.e. SqlLite

### Who do I talk to? ###

* Akshay Sharma akshay.aaxy@gmail.com